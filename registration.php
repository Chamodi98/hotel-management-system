<!DOCTYPE html>
<html>
<head>
	<title>Login and Registration Form</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<style>
		* {
			box-sizing: border-box;
		}

		body {
			font-family: Arial, sans-serif;
			background-color: #f4f4f4;
		}

		.container {
			max-width: 400px;
			margin: 0 auto;
			padding: 20px;
			background-color: #fff;
			border-radius: 5px;
			box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
		}

		h2 {
			text-align: center;
		}

		.form-group {
			margin-bottom: 20px;
		}

		.form-group label {
			display: block;
			margin-bottom: 5px;
			font-weight: bold;
		}

		.form-group input {
			width: 100%;
			padding: 10px;
			border: 1px solid #ccc;
			border-radius: 3px;
		}

		.form-group input[type="submit"] {
			background-color: #4CAF50;
			color: #fff;
			cursor: pointer;
		}

		.form-group input[type="submit"]:hover {
			background-color: #45a049;
		}

		.form-group .social-icons {
			margin-top: 10px;
		}

        .form-group .social-icons a {
	        display: inline-block;
	        width: 40px;
	        height: 40px;
	        margin-right: 10px;
	        text-align: center;
	        border-radius: 50%;
	        color: #fff;
	        background-color: #333;
	        line-height: 40px;
	        font-size: 20px;
        }

		.form-group .social-icons .fa-facebook {
			background-color: #3b5998;
		}

		.form-group .social-icons .fa-twitter {
			background-color: #55acee;
		}

		.form-group .social-icons .fa-google-plus {
			background-color: #dd4b39;
		}

		.form-group .switch-login {
			text-align: center;
			margin-top: 20px;
		}

		.form-group .switch-login a {
			color: #4CAF50;
        }
	</style>
</head>
<body>
<div class="container">
		<h2>Registration</h2>
		<form action="#" method="post">
			<div class="form-group">
				<label for="name">Name:</label>
				<input type="text" id="name" name="name" placeholder="Enter your name" required>
			</div>
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" id="email" name="email" placeholder="Enter your email" required>
			</div>
			<div class="form-group">
				<label for="password">Password:</label>
				<input type="password" id="password" name="password" placeholder="Enter your password" required>
			</div>
			<div class="form-group">
				<input type="submit" value="Register">
				<div class="social-icons">
					<a href="#" class="fa fa-facebook"></a>
					<a href="#" class="fa fa-twitter"></a>
					<a href="#" class="fa fa-google-plus"></a>
				</div>
			</div>
			<div class="switch-login">
				<p>Already have an account? <a href="login.php">Login</a></p>
			</div>
		</form>
	</div>
	</body>
</html>